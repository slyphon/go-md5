package main

import (
	"crypto/md5"
	"os"
	"flag"
	"fmt"
    "hash"
	"path/filepath"
    "time"
    "log"
    "syscall"
    "github.com/edsrzf/mmap-go"
)

const (
    BUFSIZE = 128 * 1024
)


func computeMmapDigest(fp *os.File, digest hash.Hash) (sum []byte, err os.Error) {
    mm, err := mmap.Map(fp, syscall.PROT_NONE, mmap.RDONLY)

    if mm == nil {
        return nil, err
    }

    nw, ew := digest.Write(mm[:])

    em := mm.Unmap()

    if em != nil {
        log.Println("error unmapping file!")
        return nil, em
    }

    if nw != len(mm) {
        log.Println("error updating digest: ", ew.String())
        return nil, ew
    }

    return digest.Sum(), nil

}


func computeDigest(fp *os.File, digest hash.Hash) (sum []byte, err os.Error) {
    var buf [BUFSIZE]byte

    done:
        for {
            switch nr, err := fp.Read(buf[:]); true {
            case nr < 0:
                fmt.Fprintf(os.Stderr, "error reading from file: %s\n", err.String())
                return nil, err
            case nr == 0:
                log.Println("reached EOF, returning")
                break done
            case nr > 0:
                if nw, ew := digest.Write(buf[0:nr]); nw != nr {
                    fmt.Fprintf(os.Stderr, "error writing to digest, %s\n", ew.String())
                    return nil, ew
                }
            }
        }

    return digest.Sum(), nil
}


func md5sum(path string)(hexdigest string, err os.Error) {
	digest := md5.New()
    fp, err := os.Open(path)

    if err != nil {
        return "", err
    }

    sum, err := computeMmapDigest(fp, digest)

    if e := fp.Close(); e != nil {
        return "", e
    }

    if err != nil {
        return "", err
    }

	return fmt.Sprintf("%x", sum[:]), nil
}


func main() {
	flag.Parse()
	if flag.NArg() == 0 {
		fmt.Fprintf(os.Stderr, "usage: %s /path/to/file\n", filepath.Base(os.Args[0]))
		os.Exit(1)
	}

	for i := 0; i < flag.NArg(); i++ {
		path := flag.Arg(i)

        log.Println("starting md5 of ", path)

        start_t := time.Nanoseconds()

		hexdigest, err := md5sum(path)

        duration := time.Nanoseconds() - start_t

        log.Println("md5 of ", path, " complete")

		if err != nil {
			fmt.Fprintf(os.Stderr, "md5sum: error checksumming %s: %s\n", path, err)
			os.Exit(1)
		}

		fmt.Fprintf(os.Stdout, "%s\t%s (%0.5f s)\n", hexdigest, path, float64(duration) * 1e-9)
	}
}

