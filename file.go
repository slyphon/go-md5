package file

import (
    "os"
    "syscall"
)

type File struct {
    fd int
    name string
}

func newFile(fd int, name string) *File {
    if fd < 0 {
        return nil
    }

    /* the following is equivalent to
     *
     *   n := new(File)
     *   n.fd = fd
     *   n.name = name
     *   return n
    */
    return &File{fd, name}
}

// these are visible externally
var (
    Stdin  = newFile(syscall.Stdin,  "/dev/stdin")
    Stdout = newFile(syscall.Stdout, "/dev/stdout")
    Stderr = newFile(syscall.Stderr, "/dev/stderr")
)

func OpenFile(name string, mode int, perm uint32) (file *File, err os.Error) {
    r, e := syscall.Open(name, mode, perm)
    if e != 0 {
        err = os.Errno(e)
    }
    return newFile(r, name), err
}

const (
    O_RDONLY = syscall.O_RDONLY
    O_RDWR   = syscall.O_RDWR
    O_CREATE = syscall.O_CREAT
    O_TRUNC  = syscall.O_TRUNC
)

func Open(name string) (file *File, err os.Error) {
    return OpenFile(name, O_RDONLY, 0)
}

func Create(name string) (file *File, err os.Error) {
    return OpenFile(name, O_RDWR|O_CREATE|O_TRUNC, 0666)
}

// the (file *File) is how you declare a method of a type
//
func (file *File) Close() os.Error {
    if file == nil {
        return os.EINVAL
    }

    e := syscall.Close(file.fd)

    file.fd = -1 // so it can't be closed a second time

    if e != 0 {
        return os.Errno(e)
    }
    return nil
}

func (file *File) Read(b []byte) (ret int, err os.Error) {
    if file == nil {
        return -1, os.EINVAL
    }

    r, e := syscall.Read(file.fd, b)
    if e != 0 {
        err = os.Errno(e)
    }

    return int(r), err
}

func (file *File) Write(b []byte) (ret int, err os.Error) {
    if file == nil {
        return -1, os.EINVAL
    }

    r, e := syscall.Write(file.fd, b)

    if e != 0 {
        err = os.Errno(e)
    }

    return int(r), err
}

func (file *File) Size() (ret int64, err os.Error) {
    if file == nil {
        return -1, os.EINVAL
    }

    stat := new(syscall.Stat_t)

    e := syscall.Stat(file.String(), stat)

    if e != 0 {
        err = os.Errno(e)
    }

    return stat.Size, err

}

func (file *File) String() string {
    return file.name
}

