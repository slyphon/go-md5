package md5_gs

//#cgo CFLAGS: -dynamic -bundle
//#include "md5.h"
import (
    "C"
)

import (
    "unsafe"
	"hash"
	"os"
)

type digest struct {
	state   C.gs_md5_state_t
}

func Init() hash.Hash {
	d := new(digest)
	d.Reset()
	return d
}

func (d *digest) Size() int { return 32; }

func (d *digest) Reset() {
	C.gs_md5_init(&d.state)
}

func (d *digest) Write(p []byte) (nn int, err os.Error) {
	if len(p) == 0 {
		return 0, nil
	}

	// have to worry about overflows here? blah!
	data := (*C.uchar)(unsafe.Pointer(&p[0]))

	C.gs_md5_append(&d.state, data, C.int(len(p)))

	return len(p), nil
}

func (d *digest) Sum() []byte {
	var result [16]byte
	C.gs_md5_finish(&d.state, (*C.uchar)(unsafe.Pointer(&result[0])))
	// need to unpack the result here, methinks
	return result[:]
}


