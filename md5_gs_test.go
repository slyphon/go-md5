package md5_gs

import (
    "testing"
    "bytes"
    "encoding/hex"
)



func check(t *testing.T, input string, expect string) {
    d := Init()
    input_b := make([]byte, len(input))
    copy(input_b, input)
    d.Write(input_b[:])

    result := d.Sum()

    expect_b, err := hex.DecodeString(expect)
    if err != nil {
        panic(err.String())
    }

    if !bytes.Equal(result, expect_b) {
        t.Errorf("expected != result: %q, %q", expect_b, result)
    }
}

func TestThatItWorks(t *testing.T) {
    check(t, "", "d41d8cd98f00b204e9800998ecf8427e")
    check(t, "a", "0cc175b9c0f1b6a831c399e269772661")
    check(t, "abc", "900150983cd24fb0d6963f7d28e17f72")
    check(t,"message digest", "f96b697d7cb7938d525a2f31aaf161d0")
    check(t, "abcdefghijklmnopqrstuvwxyz", "c3fcd3d76192e4007dfb496cca67e13b")
    check(t, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", "d174ab98d277d9f5a5611c2c9f419d9f")
    check(t, "12345678901234567890123456789012345678901234567890123456789012345678901234567890", "57edf4a22be3c955ac49da2e2107b67a")
}

